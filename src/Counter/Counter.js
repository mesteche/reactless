
const Increment = ({ increment, ...props }) => <button onClick={increment}>Increment!</button>
const Reset = ({ reset, ...props }) => <button onClick={reset}>Reset!</button>

const Counter = ({ value, ...props }) => (
  <div>
    <div>Clicks: {value}</div>
    <Increment />
    <Reset />
  </div>
)

export default Counter