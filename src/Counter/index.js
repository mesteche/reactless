import Component from './Counter'

const useCounter = ({ initialValue = 0, ...props }) => {
  const [ value, setState ] = props.useState(initialValue)
  const increment = () => setState(value + 1)
  const reset = () => setState(initialValue)

  return { value, increment, reset }
}

const Counter = ({ ...props }) => <Component {...useCounter(props)} />

export default Counter
