const Title = ({ title, ...props }) => <h1>{title} {props.both}</h1>

export default Title