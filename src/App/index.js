import Title from '../Title'
import Counter from '../Counter'

const App = ({ text, ...props }) => (
  <div>
    <Title />
    <span>{text} {props.both}</span>
    <Counter />
  </div>
)

export default App