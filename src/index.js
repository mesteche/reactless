import React, { useState } from 'react'
import ReactDOM from 'react-dom'

import App from './App'

const props = {
  createElement(component, props, ...children) {
    return typeof component === 'string' ?
      React.createElement(component, props, ...children) :
      React.createElement(component, Object.assign(this, props), ...children)
  }
}

ReactDOM.render(
  <App
    both="both"
    title="Title"
    text="App"
    useState={useState}
  />,
  document.getElementById('app')
)